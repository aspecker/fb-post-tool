const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PostsSchema = new Schema ({
    userID:{
        type: String,
        required: true
    },
    linkURL: {
        type: String,
        required: true
    },
    comment: {
        type: String
    }
});

var Posts = mongoose.model('Posts', PostsSchema);

module.exports = Posts;