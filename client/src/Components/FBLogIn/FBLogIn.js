import React, { Component} from 'react';
import FacebookProvider, { Login } from 'react-facebook';
 
class FBLogIn extends Component {
  handleResponse = (data) => {
    console.log(data);
  }
 
  handleError = (error) => {
    this.setState({ error });
    console.log(this.state);
  }
 
  render() {
    return (
      <FacebookProvider appId="2166045680087297">
        <Login
          scope="email"
          onResponse={this.handleResponse}
          onError={this.handleError}
        >
          <span>Login via Facebook</span>
        </Login>
      </FacebookProvider>
    );
  }
}

export default FBLogIn;