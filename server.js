const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const db = require('./models/Posts.js')
const https = require('https')
const fs = require('fs')

const app = express();
const PORT = process.env.PORT || 3001;

//config parser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// use static assets
app.use(express.static('client/build'));

// require('./routes/api-routes.js')(app)

// enable mongoose promises
mongoose.Promise = global.Promise;

// connect to DB
mongoose.connect(
    process.env.MONGODB_URI || "mongodb://localhost/fbutil"
)

// run server
https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
  }, app)
  .listen(PORT, function () {
    console.log('app listening on port 3000! Go to https://localhost:3000/')
  })